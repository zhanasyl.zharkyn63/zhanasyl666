import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Project';

  arr: any = [];


  constructor(
    private http: HttpClient
  ) {
  }
  ngOnInit() {
    this.http.get('http://jsonplaceholder.typicode.com/posts').subscribe((next: any) => {
      this.arr = next;
      console.log(next)
    })
  }
}

